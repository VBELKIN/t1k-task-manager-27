package ru.t1k.vbelkin.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.t1k.vbelkin.tm.model.Task;
import ru.t1k.vbelkin.tm.util.TerminalUtil;

public class TaskShowByIndexCommand extends AbstractTaskCommand {

    @NotNull
    public static final String NAME = "task-show-by-index";

    @NotNull
    public static final String DESCRIPTION = "Display task by index.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[SHOW TASK BY INDEX]");
        System.out.print("ENTER INDEX: ");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        @NotNull final String userId = getUserId();
        @NotNull final Task task = getTaskService().findOneByIndex(userId, index);
        showTask(task);
    }

}
