package ru.t1k.vbelkin.tm.exception.field;

public class TaskIdEmptyException extends AbstractFieldException {

    public TaskIdEmptyException() {
        super("Error! TaskId is empty...");
    }

}