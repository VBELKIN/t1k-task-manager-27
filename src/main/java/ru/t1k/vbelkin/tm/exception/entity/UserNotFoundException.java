package ru.t1k.vbelkin.tm.exception.entity;

public class UserNotFoundException extends AbstractEntityNotFoundException {

    public UserNotFoundException() {
        super("Error! User not found...");
    }

}
